<?php

use App\Http\Controllers\SimulasiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pilih_simulasi', [SimulasiController::class, 'pilih'])->name('pilih_simulasi');
Route::post('/pilih_simulasi', [SimulasiController::class, 'pilih'])->name('pilih_simulasi');
Route::get('/data_pasien', [SimulasiController::class, 'dataPasien']);
Route::get('/simulasi/{prosedur}', [SimulasiController::class, 'simulasi'])->name('simulasi');
Route::post('/cek_tindakan/{simulasi}/{tindakan}', [SimulasiController::class, 'cekTindakan'])->name('cek_tindakan');
Route::get('/selesai/{simulasi}', [SimulasiController::class, 'selesai'])->name('selesai');
Route::get('/migrate', function () {
    \Illuminate\Support\Facades\Artisan::call('migrate:fresh --seed');
    return redirect('/');
})->name('migrate');
