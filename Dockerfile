FROM composer:2 as composer
WORKDIR /app
COPY composer.lock .
COPY composer.json .
RUN mkdir -p storage/framework/cache \
    && mkdir -p storage/framework/sessions \
    && mkdir -p storage/framework/views \
    && mkdir -p storage/logs \
    && composer install --no-scripts

FROM phpswoole/swoole:4.6-alpine as base
WORKDIR /srv/www
RUN set -ex \
  && apk --no-cache add \
    postgresql-dev
RUN docker-php-ext-install pcntl \
    pdo \
    pgsql \
    pdo_pgsql

FROM composer as dump
COPY . .
RUN composer dump

FROM base as release
COPY --from=dump /app /srv/www
EXPOSE 8000
ENTRYPOINT ["php", "artisan", "octane:start"]
CMD ["--host=0.0.0.0"]
