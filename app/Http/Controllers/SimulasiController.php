<?php

namespace App\Http\Controllers;

use App\Models\Alat;
use App\Models\Prosedur;
use App\Models\RecordSimulasi;
use App\Models\Simulasi;
use App\Models\Tindakan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SimulasiController extends Controller
{
    public function pilih(Request $request)
    {
        if ($request->get('prosedur')) {
            return redirect()->route('simulasi', $request->get('prosedur'));
        }

        return view('simulasi.pilih');
    }

    public function dataPasien()
    {
        return view('simulasi.data_pasien');
    }

    public function simulasi(Request $request, Prosedur $prosedur)
    {
        Auth::setUser(User::query()->first());

        $simulasi = Simulasi::query()->create([
            'prosedur_id' => $prosedur->id,
            'user_id' => Auth::id(),
            'waktu_mulai' => Carbon::now(),
        ]);
        $step = $request->get('tindakan');

        $allAlat = Alat::query()->whereIn('id', $prosedur->tindakan()->pluck('alat_id'))->get();
        $tindakan = $prosedur->tindakan()->where('urutan', $step)->first();

        if ($tindakan === null) {
            $tindakan = $prosedur->tindakan()->first();
        }

        return view('simulasi.simulasi')->with([
            'prosedur' => $prosedur,
            'allAlat' => $allAlat,
            'tindakan' => $tindakan,
            'simulasi' => $simulasi,
        ]);
    }

    public function cekTindakan(Request $request, Simulasi $simulasi, Tindakan $tindakan)
    {
        RecordSimulasi::create([
            'simulasi_id' => $simulasi->id,
            'tindakan_id' => $tindakan->id,
            'alat_id' => $request->alat_id,
            'anatomi_id' => $request->anatomi_id,
            'waktu' => Carbon::now(),
        ]);

        if ($tindakan->alat_id == $request->alat_id
            && $tindakan->anatomi_id == $request->anatomi_id) {

            $next = Tindakan::query()->where([
                'prosedur_id' => $tindakan->prosedur_id,
                'urutan' => ((int) $tindakan->urutan) + 1,
            ])
                ->first();

            if ($next === null) {
                $simulasi->update([
                    'waktu_selesai' => Carbon::now()
                ]);
            }

            return response()->json([
                'next' => $next?->toArray(),
            ]);
        }
        $simulasi->increment('salah');
        abort('400', 'Salah');
    }

    public function selesai(Request $request, Simulasi $simulasi)
    {
        return view('simulasi.selesai')->with([
            'simulasi' => $simulasi,
        ]);
    }
}
