<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordSimulasi extends Model
{
    use HasFactory;
    protected $table = 'record_simulasi';
    protected $guarded = [];
}
