<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prosedur extends Model
{
    use HasFactory;
    protected $table = 'prosedur';

    public function tindakan()
    {
        return $this->hasMany(Tindakan::class);
    }
}
