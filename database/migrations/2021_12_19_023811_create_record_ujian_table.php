<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_ujian', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ujian_id');
            $table->bigInteger('user_id');
            $table->bigInteger('tindakan_id');
            $table->bigInteger('alat_id')->nullable();
            $table->bigInteger('anatomi_id')->nullable();
             $table->dateTime('waktu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_ujian');
    }
}
