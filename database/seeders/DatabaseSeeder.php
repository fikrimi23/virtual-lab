<?php

namespace Database\Seeders;

use App\Models\Alat;
use App\Models\Anatomi;
use App\Models\Prosedur;
use App\Models\Tindakan;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        if (User::query()->count() > 1) {
            return;
        }

        \App\Models\User::factory(10)->create();
        foreach (range(1, 20) as $item) {
            Alat::query()->create([
                'nama' => "Alat Bedah - $item",
                'foto' => "",
            ]);
        }

        foreach (range(1, 20) as $item) {
            Anatomi::query()->create([
                'nama' => "Anatomi Tubuh - $item",
                'foto' => "",
            ]);
        }

        $prosedur = Prosedur::query()->create([
            'prosedur' => 'Operasi Bypass Jantung',
        ]);

        \App\Models\User::factory(10)->create();
        $allAlat = Alat::query()->get();
        $allAnatomi = Anatomi::query()->get();
        foreach (range(1, 10) as $item) {
            $alat = $faker->randomElement($allAlat);
            $anatomi = $faker->randomElement($allAnatomi);
            Tindakan::query()->create([
                'alat_id' => $alat->id,
                'deskripsi' => sprintf("$item. Gunakan %s ke %s", $alat->nama, $anatomi->nama),
                'anatomi_id' => $anatomi->id,
                'prosedur_id' => $prosedur->id,
                'urutan' => $item,
            ]);
        }
    }
}
