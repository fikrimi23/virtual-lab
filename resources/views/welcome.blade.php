@extends('layouts.main')

@section('content')
    <a href="{{ route('migrate') }}">
        <button class="btn btn-success" {{ \Illuminate\Support\Facades\Schema::hasTable('users') ? 'disabled' : '' }}>1. Migrate Database </button>
    </a>
    <a href="{{ route('pilih_simulasi') }}">
        <button class="btn btn-success">Pilih Simulasi</button>
    </a>
@endsection
