@extends('layouts.main')

@section('content')
    <div id="pasien_paper">
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('pilih_simulasi') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <select class="form-select" name="prosedur" aria-label="Default select example">
                                    <option selected>Pilih satu</option>
                                    @foreach (\App\Models\Prosedur::all() as $prosedur)
                                        <option value="{{ $prosedur->id }}">{{ $prosedur->prosedur }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Pilih</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
