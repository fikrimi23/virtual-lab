@extends('layouts.main')

@push('css')
    <style>
        #pasien_paper {
            position: relative;
            margin-top: 20px;
            margin-left: auto;
            margin-right: auto;
            height: 450px;
            width: 800px;
            border: 1px solid;
            border-radius: 10px;
        }
    </style>
@endpush

@section('content')
    <div id="pasien_paper">
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12">
                    <fieldset disabled>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="John Doe">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Umur</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="20 Tahun">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="Laki Laki">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Prosedur Operasi</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="{{ $simulasi->prosedur->prosedur }}">
                            </div>
                        </div>
                    </fieldset>
                    <a href="{{ route('simulasi', \App\Models\Prosedur::first()) }}">
                        <button type="button" class="btn btn-success">Lanjutkan</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
