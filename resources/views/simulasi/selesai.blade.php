@extends('layouts.main')

@push('css')
    <style>
        #pasien_paper {
            position: relative;
            margin-top: 20px;
            margin-left: auto;
            margin-right: auto;
            height: 450px;
            width: 800px;
            border: 1px solid;
            border-radius: 10px;
        }
    </style>
@endpush

@section('content')
    <div id="pasien_paper">
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12">
                    <fieldset disabled>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="John Doe">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Umur</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="20 Tahun">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="Laki Laki">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Prosedur Operasi</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="Bypass Jantung">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row font-weight-bold">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Total Kesalahan Tindakan</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="staticEmail"
                                       value="{{ $simulasi->salah }}">
                            </div>
                        </div>
                        <hr>
                    </fieldset>
                    <a href="/">
                        <button type="button" class="btn btn-success">Kembali</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
