@extends('layouts.main')

@push('css')
    <style>
        #main-tubuh {
            position: absolute;
            left: 200px;
            top: 100px;
            width: 700px;
            height: 300px;
            border: 1px solid black;
            overflow: auto;
        }

        #sidebar-alat {
            position: absolute;
            left: 20px;
            top: 50px;
            width: 100px;
            height: 400px;
            border: 1px solid black;
            overflow: auto;
        }

        #topbar-tindakan {
            position: absolute;
            left: 200px;
            top: 20px;
            width: 700px;
            height: 50px;
            border: 1px solid black;
        }

        #botbar-status {
            position: absolute;
            left: 200px;
            top: 430px;
            width: 700px;
            height: 50px;
            border: 1px solid black;
            overflow: auto;
        }

        .box-alat, .box-anatomi {
            border: 1px solid black;
            margin: 2px;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <input type="hidden" name="check-url" value="{{ route('cek_tindakan', [$simulasi->id, ':id']) }}">
    <input type="hidden" name="finish-url" value="{{ route('selesai', $simulasi->id) }}">
    <input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
    <input type="hidden" name="tindakan-id" value="{{ $tindakan->id }}">
    <div id="simulasi-id">
        Simulasi ID : #{{ $simulasi->id }}
    </div>
    <div id="sidebar-alat">
        @foreach ($allAlat as $alat)
            <div class="box-alat" data-id="{{ $alat->id }}">
                {{ $alat->nama }}
                {{--<img src="{{ asset($alat->foto) }}" alt="">--}}
            </div>
        @endforeach
    </div>
    <div id="topbar-tindakan">
        Arahan:
        <span id="arahan">{{ $tindakan->deskripsi }}</span>
    </div>
    <div id="main-tubuh">
        <div class="row">
            @foreach (\App\Models\Anatomi::all() as $anatomi)
                <div class="box-anatomi col-lg-2" data-id="{{ $anatomi->id }}">
                    {{ $anatomi->nama }}
                    {{--<img src="{{ asset($alat->foto) }}" alt="">--}}
                </div>
            @endforeach
        </div>
    </div>

    <div id="botbar-status">
        <div class="col-lg-12 mt-2">
            <input name="status" type="text" style="width: 100%" disabled value="Anda tidak sedang melakukan apa apa">
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            let currentAlat = null;
            $('.box-alat').click(function () {
                $('input[name=status]').val($(this).html() + " dipilih");
                currentAlat = $(this).attr('data-id');
            });

            $('.box-anatomi').click(function () {
                if (currentAlat === null) {
                    return;
                }

                $.ajax($('input[name=check-url]').val().replace(':id', $('input[name=tindakan-id]').val()), {
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="csrf-token"]').val()
                    },
                    data: {
                        alat_id: currentAlat,
                        anatomi_id: $(this).attr('data-id'),
                    }
                })
                    .done(function (data) {
                        let next = data.next;
                        if (next === null) {
                            alert("Anda telah selesai");
                            window.location.replace($('input[name=finish-url]').val());
                            return;
                        }
                        alert("Anda benar, lanjut ke arahan berikutnya");
                        $('input[name=tindakan-id]').val(next.id);
                        $('#arahan').html(next.deskripsi);
                    })
                    .fail(function () {
                        alert("Anda salah, silahkan coba lagi");
                        $('input[name=status]').val('');
                        currentAlat = null;
                    });
            });
        });
    </script>
@endpush
